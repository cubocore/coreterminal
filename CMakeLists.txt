project( coreterminal )
cmake_minimum_required( VERSION 3.16 )

set( PROJECT_VERSION 5.0.0 )
set( PROJECT_VERSION_MAJOR 5 )
set( PROJECT_VERSION_MINOR 0 )
set( PROJECT_VERSION_PATCH 0 )

set( PROJECT_VERSION_MAJOR_MINOR ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR} )
add_compile_definitions(VERSION_TEXT="${PROJECT_VERSION}")

set( CMAKE_CXX_STANDARD 17 )
set( CMAKE_INCLUDE_CURRENT_DIR ON )
set( CMAKE_BUILD_TYPE Release )
set( CMAKE_AUTOMOC ON )
set( CMAKE_AUTORCC ON )
set( CMAKE_AUTOUIC ON )


add_definitions ( -Wall )
if ( CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT )
    set( CMAKE_INSTALL_PREFIX "/usr" CACHE PATH "Location for installing the project" FORCE )
endif()

add_compile_definitions(PREFIX_PATH="${CMAKE_INSTALL_PREFIX}")

find_package ( Qt6Widgets REQUIRED )
find_package ( Qt6Core REQUIRED )
find_package ( Qt6Gui REQUIRED )
find_package ( Qt6SerialPort REQUIRED )
find_package ( qtermwidget6 REQUIRED )

set ( SOURCES
    src/global.cpp
    src/main.cpp
    src/coreterminal.h
    src/coreterminal.cpp
    src/cserialtermwidget.h
    src/cserialtermwidget.cpp
    src/ctermwidget.h
    src/ctermwidget.cpp
    src/ctabwidget.h
    src/ctabwidget.cpp
    src/settings.h
    src/settings.cpp
)

set ( UIS
    src/CSerialTermWidget.ui
)

add_executable ( coreterminal ${SOURCES} ${UIS} )
target_include_directories(coreterminal PRIVATE src)
target_link_libraries ( coreterminal  Qt6::Core Qt6::Gui Qt6::Widgets Qt6::SerialPort qtermwidget6 cprime-widgets cprime-core )

# Installation: Main Executable
install( TARGETS coreterminal DESTINATION bin )
install( FILES cc.cubocore.CoreTerminal.desktop DESTINATION share/applications )
install( FILES cc.cubocore.CoreTerminal.svg DESTINATION share/icons/hicolor/scalable/apps/ )
