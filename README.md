# CoreTerminal
A terminal emulator for C Suite.
Based on [qtermwidget](https://github.com/lxqt/qtermwidget).

<img src="coreterminal.png" width="500">

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/coreapps/coreterminal/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)
* [DebPackages](https://gitlab.com/cubocore/wiki/-/tree/master/DebPackages)
* [Gentoo](https://gitweb.gentoo.org/repo/proj/guru.git/tree/gui-apps/coreterminal)
* [AppImages](https://gitlab.com/cubocore/wiki/-/tree/master/AppImages)

### Dependencies:
* qt6-base
* qt6-serialport
* [libcprime](https://gitlab.com/cubocore/libcprime) 
* [qtermwidget](https://github.com/lxqt/qtermwidget) 

### Information
Please see the [Wiki page](https://gitlab.com/cubocore/wiki) for this info.
* Changes in latest release ([ChangeLog](https://gitlab.com/cubocore/wiki/-/blob/master/ChangeLog))
* Build from the source ([BuildInfo](https://gitlab.com/cubocore/wiki/blob/master/BuildInfo.md))
* Tested In ([Test System](https://gitlab.com/cubocore/wiki/blob/master/TestSystem))
* Known Bugs ([Current list of issues](https://gitlab.com/groups/cubocore/coreapps/-/issues))
* Help Us

### Feedback
* We need your feedback to improve the C Suite. Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/coreapps/-/issues).
  
  Or feel free to join and chat with us in IRC/Matrix #cubocore:matrix.org or [Element.io](https://app.element.io/#/room/#cubocore:matrix.org)
s
