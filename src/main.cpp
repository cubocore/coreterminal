/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#include <QApplication>

#include <iostream>
#include <sys/utsname.h>

#include "coreterminal.h"
#include "settings.h"

const char *platform()
{
    struct utsname buffer
    {};

    uname(&buffer);

    QString Qt4Version = QString("Powered by Qt ") + QT_VERSION_STR + " on Linux " + buffer.release;
    QString GCCVersion = QString("Compiled with GCC ") + __VERSION__;

    QString version = Qt4Version + '\n' + GCCVersion;

    return qPrintable(version);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    // Set application info
    app.setOrganizationName("CuboCore");
    app.setApplicationName("CoreTerminal");
    app.setApplicationVersion(QStringLiteral(VERSION_TEXT));
    app.setDesktopFileName("cc.cubocore.CoreTerminal.desktop");
    app.setQuitOnLastWindowClosed(true);

    for (int i = 1; i < argc; i++) {
        if ((QString(argv[i]) == QString("--help")) or (QString(argv[i]) == QString("-h"))) {
            std::cout << "CoreTerminal v" << VERSION_TEXT << "\n" << std::endl;
            std::cout << "Usage: CoreTerminal [options]" << std::endl;
            std::cout << "Options:" << std::endl;
            std::cout << "  -h|--help                         Print this help" << std::endl;
            std::cout << "  -v|--version                      Prints application version and exits"
                      << std::endl;
            std::cout
                << "  -w|--working-directory <dir>      Start session with specified work directory"
                << std::endl;
            std::cout << "  -e|--execute <command>            Execute command in the shell"
                      << std::endl;

            return 0;
        }

        if ((QString(argv[i]) == QString("--version")) or (QString(argv[i]) == QString("-v"))) {
            std::cout << "CoreTerminal v" << VERSION_TEXT << "\n" << std::endl;
            std::cout << platform() << std::endl;

            return 0;
        }
    }

    settings smi;
    CoreTerminal Gui(&smi);
    Gui.show();

    return app.exec();
}
