/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#include <cmath>
#include <QApplication>
#include <QCloseEvent>
#include <QDir>
#include <QFileInfo>
#include <QFontMetrics>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QScreen>
#include <QStyle>
#include <QTabBar>
#include <QWidget>

#include <cprime/appopenfunc.h>
#include <cprime/themefunc.h>

#include "cserialtermwidget.h"
#include "ctabwidget.h"
#include "ctermwidget.h"
#include "global.h"

#include "coreterminal.h"

CoreTerminal::CoreTerminal(settings *s, QWidget *parent)
    : QMainWindow(parent)
    , uiMode(0)
    , mRows(30)
    , mCols(120)
    , smi(s)
{
    createGUI();
    setupActions();

    loadSettings();

    setWindowProperties();

    if (not globalObj::watcher()->files().contains(smi->defaultSettingsFilePath())) {
        globalObj::watcher()->addPath(smi->defaultSettingsFilePath());
    }

    connect(globalObj::watcher(),
            &QFileSystemWatcher::fileChanged,
            this,
            &CoreTerminal::reloadSettings);
}

CoreTerminal::~CoreTerminal()
{
    delete fm;
    delete TabWidget;
}

void CoreTerminal::loadSettings()
{
    // get CSuite's settings
    uiMode = smi->getValue("CoreStats", "UIMode");

    // get app's settings
    windowMaximized = smi->getValue("CoreTerminal", "WindowMaximized");
    mRows = smi->getValue("CoreTerminal", "Rows", 30);
    mCols = smi->getValue("CoreTerminal", "Columns", 120);

    QFont tFont = smi->getValue("CoreTerminal", "Font");
    fm = new QFontMetrics(tFont);
}

void CoreTerminal::createGUI()
{
    /* Our tab widget */
    TabWidget = new CTabWidget(smi, this);
    connect(TabWidget, &CTabWidget::changeWindowTitle, this, &QMainWindow::setWindowTitle);

    /* Base Layout */
    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setSpacing(0);
    lyt->setContentsMargins(QMargins());
    lyt->addWidget(TabWidget);

    /* Base Widget */
    QWidget *widget = new QWidget();

    widget->setObjectName("base");
    widget->setLayout(lyt);

    setCentralWidget(widget);

    /* Start a terminal session */
    startTerminal();

    /* Widget Properties */
    TabWidget->setFocusPolicy(Qt::NoFocus);
}

void CoreTerminal::startTerminal()
{
    QStringList argv = qApp->arguments();
    QString workDir;
    QString exeArgs;

    int dirIdx = argv.indexOf("--working-directory") & argv.indexOf("-w");
    int exeIdx = argv.indexOf("--execute") & argv.indexOf("-e");

    if ((dirIdx == -1) and (exeIdx == -1)) {
        TabWidget->newTerminal();
        return;
    }

    if (dirIdx < 0) {
        // --workdir not used
        workDir = QDir::currentPath();
    } else if ((dirIdx + 1 >= argv.size()) or (dirIdx + 1 == exeIdx)) {
        // --workdir used but not specified
        workDir = QDir::currentPath();
    } else {
        // --workdir used and specified
        workDir = argv.at(dirIdx + 1);
    }

    if (exeIdx < 0) {
        // --execute not used
        exeArgs = QString();
    } else if ((exeIdx + 1 >= argv.size()) or (exeIdx + 1 == dirIdx)) {
        // --execute used but not specified
        exeArgs = QString();
    } else {
        // --execute used and specified
        if (dirIdx < exeIdx) {
            // --workdir was specified before --execute
            for (int i = exeIdx + 1; i < argv.size(); i++) {
                exeArgs += QString("%1 ").arg(argv.at(i));
            }
        } else {
            for (int i = exeIdx + 1; i < dirIdx; i++) {
                exeArgs += QString("%1 ").arg(argv.at(i));
            }

            exeArgs += "&";
        }
    }

    TabWidget->newTerminal(workDir, exeArgs);
}

void CoreTerminal::setupActions()
{
    connect(TabWidget, SIGNAL(close()), this, SLOT(close()));

    // New Terminal
    QAction *newTermAct = new QAction("&New Terminal", this);

    newTermAct->setShortcuts(QList<QKeySequence>() << tr("Ctrl+Shift+N"));

    connect(newTermAct, SIGNAL(triggered()), TabWidget, SLOT(newTerminalWindow()));
    addAction(newTermAct);

    // New Terminal in the current  directory
    QAction *newTermCwdAct = new QAction("&New Terminal in CWD", this);

    newTermCwdAct->setShortcuts(QList<QKeySequence>() << tr("Ctrl+Shift+T"));

    connect(newTermCwdAct, SIGNAL(triggered()), TabWidget, SLOT(newTerminalCWD()));
    addAction(newTermCwdAct);

    // Clear Terminal
    QAction *clearTermAct = new QAction("C&lear Terminal", this);

    clearTermAct->setShortcuts(QList<QKeySequence>() << tr("Ctrl+Shift+X"));

    connect(clearTermAct, SIGNAL(triggered()), TabWidget, SLOT(clearTerminal()));
    addAction(clearTermAct);

    // Copy Selection
    QAction *copyAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-copy-symbolic",
                                                                "edit-copy",
                                                                "edit-copy"),
                                   "&Copy",
                                   this);

    copyAct->setShortcut(tr("Ctrl+Shift+C"));

    connect(copyAct, SIGNAL(triggered()), TabWidget, SLOT(copyToClipboard()));
    addAction(copyAct);

    // Paste Clipboard
    QAction *pasteAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-paste-symbolic",
                                                                 "edit-paste",
                                                                 "edit-paste"),
                                    "&Paste",
                                    this);

    pasteAct->setShortcut(tr("Ctrl+Shift+V"));

    connect(pasteAct, SIGNAL(triggered()), TabWidget, SLOT(pasteClipboard()));
    addAction(pasteAct);

    // Previous Terminal
    QAction *prevTermAct = new QAction("&prev Terminal", this);

    prevTermAct->setShortcuts(QList<QKeySequence>()
                              << tr("Ctrl+Shift+Tab") << tr("Ctrl+PgUp") << tr("Shift+Left"));

    connect(prevTermAct, SIGNAL(triggered()), TabWidget, SLOT(prevTerminal()));
    addAction(prevTermAct);

    // Next Terminal
    QAction *nextTermAct = new QAction("&Next Terminal", TabWidget);

    nextTermAct->setShortcuts(QList<QKeySequence>()
                              << tr("Ctrl+Tab") << tr("Ctrl+PgDown") << tr("Shift+Right"));

    connect(nextTermAct, SIGNAL(triggered()), TabWidget, SLOT(nextTerminal()));
    addAction(nextTermAct);

    // Open FileManager here
    QAction *fmgrAct = new QAction("Open &File Manager", this);

    fmgrAct->setShortcuts(QList<QKeySequence>() << tr("Ctrl+Shift+O"));

    connect(fmgrAct, SIGNAL(triggered()), this, SLOT(openFMgr()));
    addAction(fmgrAct);

    // closeTab CoreTerminal
    QAction *closeTabAct = new QAction("Close &Tab", this);

    closeTabAct->setShortcuts(QList<QKeySequence>() << tr("Ctrl+Shift+W"));

    connect(closeTabAct, SIGNAL(triggered()), TabWidget, SLOT(closeTab()));
    addAction(closeTabAct);
}

void CoreTerminal::setWindowProperties()
{
    setWindowTitle("CoreTerminal");
    setWindowIcon(QIcon::fromTheme("cc.cubocore.CoreTerminal"));

    setAttribute(Qt::WA_TranslucentBackground);
    setStyleSheet("#base { background-color: palette(Window); }");

    setBaseSize((QApplication::style()->styleHint(QStyle::SH_ScrollBar_Transient)
                     ? 3
                     : QApplication::style()->pixelMetric(QStyle::PM_ScrollBarExtent) + 2),
                TabWidget->tabBar()->tabRect(0).height() + 3);

    auto screenSize = QApplication::primaryScreen()->availableSize();
    int w = qMin(screenSize.width(), getWidthByColumn(mCols) + baseSize().width());
    int h = qMin(screenSize.height(), getHeightByRow(mRows) + baseSize().height());

    resize(w, h);

    setSizeIncrement(fm->averageCharWidth(), fm->height());

    if (uiMode != 0) {
        setWindowFlags(windowFlags());
        setWindowState(windowState() | Qt::WindowMaximized | Qt::WindowActive);
    }
}

/*
  * @outpaddling Perfect size =
  * tabBar->height() +7 for Fusion, +4 for QtCurve, +3 for Windows tabBar->tabRect(0).height() seems to align:
  * 36 for Fusion, 33 for QtCurve, 32 for Windows using 12pt font Also verified with several other styles and
  * font sizes
  */
/*
  * Tab bar height and scroll bar width is handled by base size
  */
int CoreTerminal::getHeightByRow(int row)
{
    return fm->height() * row;
}

int CoreTerminal::getWidthByColumn(int column)
{
    return fm->averageCharWidth() * column;
}

int CoreTerminal::getRowsCount()
{
    return (size().height() - baseSize().height()) / fm->height();
}

int CoreTerminal::getColumnsCount()
{
    return (size().width() - baseSize().width()) / fm->averageCharWidth();
}

void CoreTerminal::showHide()
{
    if (isVisible()) {
        hide();
    } else {
        show();
        activateWindow();
    }
}

void CoreTerminal::openFMgr()
{
    QString cwd = qobject_cast<CTermWidget *>(TabWidget->currentWidget())->currentWorkingDirectory();

    CPrime::AppOpenFunc::defaultAppEngine(CPrime::FileManager, QFileInfo(cwd), QString());
}

void CoreTerminal::reloadSettings()
{
    loadSettings();
}

void CoreTerminal::closeEvent(QCloseEvent *cEvent)
{
    qDebug() << "save window stats" << getRowsCount() << getColumnsCount() << this->isMaximized();
    smi->setValue("CoreTerminal", "Rows", getRowsCount());
    smi->setValue("CoreTerminal", "Columns", getColumnsCount());
    smi->setValue("CoreTerminal", "WindowMaximized", this->isMaximized());

    if (!TabWidget->count()) {
        return;
    }

    // QString msg = "Would you like to close %1 tab%2?\nThere might be some process opened right now.";

    // msg = msg.arg((TabWidget->count() > 1) ? QString::number(TabWidget->count()) : "current",
    // (TabWidget->count() > 1) ? "s" : "");

    // int ret = QMessageBox::question(this, "Close", msg, QMessageBox::Yes | QMessageBox::No,
    // QMessageBox::No);

    // if (ret == QMessageBox::No) {
    // cEvent->ignore();
    // return;
    // }

    // for (int i = 0; i < TabWidget->count(); i++) {
    // auto tw = qobject_cast<CTermWidget *>(TabWidget->widget(i));

    // if (tw) {
    // tw->sendText(QString::fromLocal8Bit("exit\n"));
    // continue;
    // }

    // auto st = qobject_cast<CSerialTermWidget *>(TabWidget->widget(i));

    // if (st) {
    // st->disconnectPort();
    // }
    // }

    /** Collect the term widgets */
    QList<CTermWidget *> terms;
    QList<CSerialTermWidget *> serials;

    bool isSafeToClose = false;

    for (int i = 0; i < TabWidget->count(); i++) {
        isSafeToClose |= TabWidget->isSafeToClose(i);
    }

    if (!isSafeToClose) {
        int reply = QMessageBox::question(
            this,
            "CoreTerminal | Close Running Processes?",
            "One or more of the tabs have a foreground process running in them or have an active "
            "serial connection open. Closing these tabs will kill the process and/or close the "
            "connection. "
            "<p>Kill the running processes and close active connections?</p>",
            QMessageBox::Yes | QMessageBox::No,
            QMessageBox::No);

        if (reply == QMessageBox::No) {
            cEvent->ignore();
            return;
        }
    }

    for (int i = 0; i < TabWidget->count(); i++) {
        /** No confirmation needed */
        TabWidget->closeTab(i, false);
    }

    cEvent->accept();
}
