/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#include <QDebug>
#include <QIntValidator>
#include <QLineEdit>
#include <QMessageBox>
#include <QScrollBar>
#include <QSerialPortInfo>
#include <QVariant>

#include "cserialtermwidget.h"
#include "ui_CSerialTermWidget.h"

QVariant val(QComboBox *box)
{
    return box->itemData(box->currentIndex());
}

CSerialTermWidget::CSerialTermWidget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::CSerialTermWidget)
{
    ui->setupUi(this);

    m_serial = new QSerialPort(this);
    ui->inputtxt->setReadOnly(true);

    /* Set mono font at input and output */
    QFont font = QFontDatabase::systemFont(QFontDatabase::FixedFont);

    if (font.family().length()) {
        font = QFont("monospace", 9);
    }

    ui->outputtxt->setFont(font);
    ui->inputtxt->setFont(font);

    /* Serial port signal connect */
    connect(m_serial, &QSerialPort::errorOccurred, this, &CSerialTermWidget::handleError);
    connect(m_serial, &QSerialPort::readyRead, this, &CSerialTermWidget::readData);
    connect(ui->outputtxt, &ConsoleText::inputData, this, &CSerialTermWidget::writeData);

    /* Get ports Info */
    ui->serialPorts->clear();
    const auto info = QSerialPortInfo::availablePorts();

    for (auto const &port : info) {
        QString str = (port.description().length() ? port.description() : "N/A") + "\n"
                      + (port.manufacturer().length() ? port.manufacturer() : "N/A") + "\n"
                      + (port.serialNumber().length() ? port.serialNumber() : "N/A") + "\n"
                      + port.systemLocation() + "\n"
                      + (port.vendorIdentifier() ? QString::number(port.vendorIdentifier(), 16)
                                                 : "N/A")
                      + "\n"
                      + (port.productIdentifier() ? QString::number(port.productIdentifier(), 16)
                                                  : "N/A");

        ui->serialPorts->addItem(port.portName(), str);
    }

    ui->serialPorts->addItem("Custom");

    /* Set ports info */
    ui->baudRate->addItem("1200", QSerialPort::Baud1200);
    ui->baudRate->addItem("2400", QSerialPort::Baud2400);
    ui->baudRate->addItem("4800", QSerialPort::Baud4800);
    ui->baudRate->addItem("9600", QSerialPort::Baud9600);
    ui->baudRate->addItem("19200", QSerialPort::Baud19200);
    ui->baudRate->addItem("38400", QSerialPort::Baud38400);
    ui->baudRate->addItem("57600", QSerialPort::Baud57600);
    ui->baudRate->addItem("115200", QSerialPort::Baud115200);
    ui->baudRate->addItem("Custom");
    ui->baudRate->setCurrentIndex(3);

    ui->dataBits->addItem("5", QSerialPort::Data5);
    ui->dataBits->addItem("6", QSerialPort::Data6);
    ui->dataBits->addItem("7", QSerialPort::Data7);
    ui->dataBits->addItem("8", QSerialPort::Data8);
    ui->dataBits->setCurrentIndex(3);

    ui->parity->addItem("None", QSerialPort::NoParity);
    ui->parity->addItem("Even", QSerialPort::EvenParity);
    ui->parity->addItem("Odd", QSerialPort::OddParity);
    ui->parity->addItem("Mark", QSerialPort::MarkParity);
    ui->parity->addItem("Space", QSerialPort::SpaceParity);

    ui->stopBits->addItem("1", QSerialPort::OneStop);
    ui->stopBits->addItem("2", QSerialPort::TwoStop);

    ui->flowControl->addItem("None", QSerialPort::NoFlowControl);
    ui->flowControl->addItem("RTS/CTS", QSerialPort::HardwareControl);
    ui->flowControl->addItem("XON/XOFF", QSerialPort::SoftwareControl);

    ui->inputtxt->setFixedHeight(30);
    ui->gConnection->setVisible(true);
    ui->gIO->setVisible(false);
}

CSerialTermWidget::~CSerialTermWidget()
{
    delete ui;
}

QString CSerialTermWidget::title()
{
    return ui->serialPorts->currentText() + " Port";
}

void CSerialTermWidget::on_serialPorts_currentIndexChanged(int index)
{
    if (index < 0) {
        return;
    }

    QString str;
    QVariant data = ui->serialPorts->itemData(index);
    ui->serialPorts->setEditable(!data.isValid());

    if (data.isValid()) {
        str = ui->serialPorts->itemData(index).toString();
    } else {
        str = "N/A\nN/A\nN/A\nN/A\nN/A\nN/A";
        ui->serialPorts->clearEditText();
    }

    ui->portInfotxt->setText(str);
}

void CSerialTermWidget::on_serialPorts_currentTextChanged(const QString &arg1)
{
    Q_UNUSED(arg1)
    emit titleChanged();
}

void CSerialTermWidget::closeEvent(QCloseEvent *event)
{
    qDebug() << "Closing " << title();
    disconnectPort();

    event->accept();
}

void CSerialTermWidget::connectPort()
{
    m_serial->setPortName(ui->serialPorts->currentText());
    m_serial->setBaudRate(val(ui->baudRate).value<QSerialPort::BaudRate>());
    m_serial->setDataBits(val(ui->dataBits).value<QSerialPort::DataBits>());
    m_serial->setParity(val(ui->parity).value<QSerialPort::Parity>());
    m_serial->setStopBits(val(ui->stopBits).value<QSerialPort::StopBits>());
    m_serial->setFlowControl(val(ui->flowControl).value<QSerialPort::FlowControl>());

    if (m_serial->open(QIODevice::ReadWrite)) {
        ui->gConnection->setVisible(false);
        ui->gIO->setVisible(true);
        mIsConnected = true;
    } else {
        QMessageBox::critical(this, tr("Error"), m_serial->errorString());
        ui->toggleConnect->setChecked(false);
        ui->toggleConnect->setText("Connect");
        mIsConnected = false;
    }
}

bool CSerialTermWidget::isConnected()
{
    return mIsConnected;
}

void CSerialTermWidget::disconnectPort()
{
    qDebug() << "Closing Port";

    if (m_serial->isOpen()) {
        m_serial->close();
    }

    mIsConnected = false;
}

void CSerialTermWidget::on_baudRate_currentIndexChanged(int index)
{
    if (index < 0) {
        return;
    }

    bool isCustom = !ui->baudRate->itemData(index).isValid();
    ui->baudRate->setEditable(isCustom);

    if (isCustom) {
        ui->baudRate->clearEditText();
        QLineEdit *edit = ui->baudRate->lineEdit();
        edit->setValidator(new QIntValidator(0, 4000000));
    }
}

void CSerialTermWidget::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) {
        QMessageBox::critical(this, "Error", m_serial->errorString());
        disconnectPort();
    }
}

void CSerialTermWidget::readData()
{
    const QByteArray data = m_serial->readAll();

    ui->outputtxt->insertPlainText(data);
    ui->outputtxt->ensureCursorVisible();
}

void CSerialTermWidget::writeData(const QByteArray &data)
{
    m_serial->write(data);
}

ConsoleText::ConsoleText(QWidget *parent)
    : QPlainTextEdit(parent)
{
    QPalette pal = palette();

    pal.setColor(QPalette::Base, Qt::black);
    pal.setColor(QPalette::Text, Qt::green);
    setPalette(pal);
}

void ConsoleText::keyPressEvent(QKeyEvent *event)
{
    switch (event->key()) {
    case Qt::Key_Backspace:
    case Qt::Key_Left:
    case Qt::Key_Right:
    case Qt::Key_Up:
    case Qt::Key_Down:
        break;

    default:
        QPlainTextEdit::keyPressEvent(event);
        emit inputData(event->text().toLocal8Bit());
    }
}

void ConsoleText::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
}

void ConsoleText::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED(event)
}

void ConsoleText::contextMenuEvent(QContextMenuEvent *event)
{
    Q_UNUSED(event)
}

void CSerialTermWidget::on_toggleConnect_clicked()
{
    connectPort();
}
