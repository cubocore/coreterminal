/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#pragma once

#include <QTabWidget>

#include "settings.h"

class QToolButton;

class CTabWidget : public QTabWidget
{
    Q_OBJECT

public:
    CTabWidget(settings *s, QWidget *parent);
    ~CTabWidget();

    /** Check if there is an active connection or running process? */
    bool isSafeToClose(int);

private:
    int uiMode;
    QSize toolsIconSize;
    settings *smi;

    void loadSettings();

public slots:
    bool newTerminalWindow();
    int newTerminal();
    int newTerminal(const QString &, const QString &cmd);
    int newTerminalCWD();

    int newSerialTerm();

    void clearTerminal();
    void copyToClipboard();
    void pasteClipboard();
    void prevTerminal();
    void nextTerminal();
    void closeTab();

    /**
	  * Confirm that this tab should be closed. This confirmation is asked only if there is an active
	  * connection or a foreground process.
	  */
    void closeTab(int, bool confirm = true);

    void toggleSearch();

private slots:
    void printSelection(bool);
    void renameTab(const QString &text, int idx);
    void changeTitle(int index);

Q_SIGNALS:
    void close();
    void changeWindowTitle(QString);
};
