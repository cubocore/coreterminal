/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#include <QApplication>
#include <QClipboard>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QProcess>
#include <QToolButton>

#include <cprime/filefunc.h>
#include <cprime/themefunc.h>

#include "cserialtermwidget.h"
#include "ctermwidget.h"
#include "global.h"

#include "ctabwidget.h"

CTabWidget::CTabWidget(settings *s, QWidget *parent)
    : QTabWidget(parent)
    , uiMode(0)
    , smi(s)
{
    loadSettings();

    setMovable(true);
    setTabsClosable(true);
    setDocumentMode(true);

    setObjectName("tabs");

    setFocusPolicy(Qt::NoFocus);
    setIconSize(toolsIconSize);

    connect(this, &QTabWidget::tabCloseRequested, [=](int idx) {
        // Confirm that this tab can be closed safely
        closeTab(idx, true);
    });

    connect(this, &CTabWidget::currentChanged, this, &CTabWidget::changeTitle);

    setTabPosition(QTabWidget::North);

    /* New tab button */
    QToolButton *newTabBtn = new QToolButton();
    newTabBtn->setIcon(CPrime::ThemeFunc::themeIcon("utilities-terminal-symbolic",
                                                    "utilities-terminal",
                                                    "utilities-terminal"));
    newTabBtn->setIconSize(toolsIconSize);
    newTabBtn->setAutoRaise(true);
    newTabBtn->setFocusPolicy(Qt::NoFocus);
    newTabBtn->setToolTip("New Tab");

    /* Search button */
    QToolButton *searchBtn = new QToolButton();
    searchBtn->setIcon(CPrime::ThemeFunc::themeIcon("edit-find-symbolic", "edit-find", "edit-find"));
    searchBtn->setIconSize(toolsIconSize);
    searchBtn->setAutoRaise(true);
    searchBtn->setFocusPolicy(Qt::NoFocus);
    searchBtn->setToolTip("Search here");

    /* New Serial terminal */
    QToolButton *serialTermBtn = new QToolButton();
    serialTermBtn->setIcon(
        CPrime::ThemeFunc::themeIcon("network-wired-symbolic", "network-wired", "network-wired"));
    serialTermBtn->setIconSize(toolsIconSize);
    serialTermBtn->setAutoRaise(true);
    serialTermBtn->setFocusPolicy(Qt::NoFocus);
    serialTermBtn->setToolTip("New Serial terminal connection");

    QHBoxLayout *lyt = new QHBoxLayout();
    lyt->setContentsMargins(QMargins());
    lyt->setSpacing(0);
    lyt->addWidget(newTabBtn);
    lyt->addWidget(serialTermBtn);
    lyt->addWidget(searchBtn);

    QWidget *cornerWidget = new QWidget();
    cornerWidget->setLayout(lyt);

    connect(serialTermBtn, &QToolButton::clicked, this, &CTabWidget::newSerialTerm);
    connect(newTabBtn, SIGNAL(clicked()), this, SLOT(newTerminal()));
    connect(searchBtn, SIGNAL(clicked()), this, SLOT(toggleSearch()));

    setCornerWidget(cornerWidget, Qt::TopRightCorner);
}

CTabWidget::~CTabWidget()
{
    for (int i = 0; i < count(); i++) {
        closeTab(i);
    }
}

/**
  * @brief Check if there is an active connection or running process.
  */
bool CTabWidget::isSafeToClose(int idx)
{
    CTermWidget *term = qobject_cast<CTermWidget *>(widget(idx));

    if (term) {
        /** true when the shell is the foreground process */
        return (term->getForegroundProcessId() == term->getShellPID());
    }

    CSerialTermWidget *serial = qobject_cast<CSerialTermWidget *>(widget(idx));

    if (serial) {
        /** true when serial is not connected */
        return (serial->isConnected() == false);
    }

    return true;
}

/**
  * @brief Loads application settings
  */
void CTabWidget::loadSettings()
{
    // get CSuite's settings
    toolsIconSize = smi->getValue("CoreStats", "ToolsIconSize");
    uiMode = smi->getValue("CoreStats", "UIMode");
}

bool CTabWidget::newTerminalWindow()
{
    QString program = qApp->arguments().at(0);
    auto curTerm = qobject_cast<CTermWidget *>(currentWidget());
    QString cwd = curTerm->currentWorkingDirectory();

    return QProcess::startDetached(program, {"--working-directory", cwd}, cwd);
}

void CTabWidget::renameTab(const QString &text, int idx)
{
    emit changeWindowTitle("CoreTerminal - " + text);

    QString tabText = CPrime::FileUtils::baseName(text);

    if (uiMode == 2) {
        tabText = QString::number(idx);
    }

    setTabText(idx, tabText);
}

void CTabWidget::changeTitle(int index)
{
    emit changeWindowTitle("CoreTerminal - " + tabText(index));
}

int CTabWidget::newTerminal()
{
    CTermWidget *widget = new CTermWidget(smi, this);

    widget->setFocusPolicy(Qt::StrongFocus);

    int idx = addTab(widget,
                     CPrime::ThemeFunc::themeIcon("utilities-terminal-symbolic",
                                                  "utilities-terminal",
                                                  "utilities-terminal"),
                     "");
    connect(widget, SIGNAL(finished()), this, SLOT(closeTab()));

#ifndef OLD_QTERMWIDGET
    connect(widget, &QTermWidget::titleChanged, [=]() { renameTab(widget->title(), idx); });
#endif

    setCurrentIndex(idx);
    widget->setFocus();

    return idx;
}

int CTabWidget::newTerminal(const QString &wDir, const QString &cmd)
{
    CTermWidget *widget = new CTermWidget(smi, this, wDir, cmd);

    widget->setFocusPolicy(Qt::StrongFocus);

    int idx = addTab(widget,
                     CPrime::ThemeFunc::themeIcon("utilities-terminal-symbolic",
                                                  "utilities-terminal",
                                                  "eutilities-terminal"),
                     "");
    connect(widget, SIGNAL(finished()), this, SLOT(closeTab()));

#ifndef OLD_QTERMWIDGET
    connect(widget, &QTermWidget::titleChanged, [=]() { renameTab(widget->title(), idx); });
#endif

    setCurrentIndex(idx);
    widget->setFocus();

    return idx;
}

int CTabWidget::newTerminalCWD()
{
    CTermWidget *curTerm = qobject_cast<CTermWidget *>(currentWidget());
    QString cwd = curTerm->currentWorkingDirectory();

    return newTerminal(cwd, "");
}

int CTabWidget::newSerialTerm()
{
    CSerialTermWidget *sterm = new CSerialTermWidget(this);

    sterm->setFocusPolicy(Qt::StrongFocus);

    int idx = addTab(sterm,
                     CPrime::ThemeFunc::themeIcon("network-wired-symbolic",
                                                  "network-wired",
                                                  "network-wired"),
                     sterm->title());
    connect(sterm, &CSerialTermWidget::titleChanged, [=]() { renameTab(sterm->title(), idx); });

    renameTab(sterm->title(), idx);
    setCurrentIndex(idx);
    sterm->setFocus();

    return idx;
}

void CTabWidget::clearTerminal()
{
    auto widget = qobject_cast<CTermWidget *>(currentWidget());

    widget->clear();
}

void CTabWidget::copyToClipboard()
{
    auto widget = qobject_cast<CTermWidget *>(currentWidget());

    widget->copyClipboard();
}

void CTabWidget::pasteClipboard()
{
    auto widget = qobject_cast<CTermWidget *>(currentWidget());

    widget->pasteClipboard();
}

void CTabWidget::prevTerminal()
{
    int idx = currentIndex();

    if (idx == 0) {
        setCurrentIndex(count() - 1);
    } else {
        setCurrentIndex(idx - 1);
    }

    emit changeWindowTitle("CoreTerminal - " + tabText(idx - 1));
}

void CTabWidget::nextTerminal()
{
    int idx = currentIndex();

    if (idx == count() - 1) {
        setCurrentIndex(0);
    } else {
        setCurrentIndex(idx + 1);
    }

    emit changeWindowTitle("CoreTerminal - " + tabText(idx + 1));
}

void CTabWidget::closeTab(int tabIndex, bool confirm)
{
    /** Get the instance of CSerialTermWidget */
    auto serial = qobject_cast<CSerialTermWidget *>(widget(tabIndex));

    /** Check only if we have to confirm */
    if (confirm) {
        /** Active connection or running process */
        if (isSafeToClose(tabIndex) == false) {
            QString text(serial
                             ? "has an active serial connection. The connection will be terminated."
                             : "The process will be terminated.");
            int reply = QMessageBox::question(this,
                                              "CoreTerminal | Close Running Process?",
                                              "The tab you are about to close " + text
                                                  + "<p>Proceed?</p>",
                                              QMessageBox::Yes | QMessageBox::No,
                                              QMessageBox::No);

            /** The user does not want to close connect/kill the process. */
            if (reply == QMessageBox::No) {
                return;
            }
        }
    }

    // Disconnect the serial port
    if (serial) {
        serial->disconnectPort();
    }

    auto tw = qobject_cast<CTermWidget *>(widget(tabIndex));

    // Sending "exit\n" may work for a shell, but is unnecessary.
    // Sending "exit\n" to any other process is useless.
    // if (tw) {
    // tw->sendText(QString::fromLocal8Bit("exit\n"));
    // }

    // This will send SIGHUP to the underlying process.
    // If the process does not respond, it will be killed.
    delete tw;

    removeTab(tabIndex);

    if (count() == 0) {
        emit close();
    }
}

void CTabWidget::closeTab()
{
    if (qobject_cast<CTermWidget *>(sender())) {
        removeTab(indexOf(qobject_cast<CTermWidget *>(sender())));
    } else {
        removeTab(currentIndex());
    }

    if (count() == 0) {
        emit close();
    }
}

void CTabWidget::toggleSearch()
{
    auto widget = qobject_cast<CTermWidget *>(currentWidget());

    if (widget) {
        widget->toggleShowSearchBar();
    }
}

void CTabWidget::printSelection(bool selection)
{
    auto widget = qobject_cast<CTermWidget *>(currentWidget());

    widget->copyClipboard();

    QClipboard *klipper = qApp->clipboard();
    qDebug() << klipper->text();

    if (selection) {
        widget->pasteSelection();
    }
}
