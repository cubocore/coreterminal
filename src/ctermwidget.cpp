/*
  *
  * This file is a part of CoreTerminal. A terminal emulator for C Suite. Copyright 2019 CuboCore Group
  *
  *
  *
  * This program is free software; you can redistribute it and/or modify it under the terms of the GNU General
  * Public License as published by the Free Software Foundation; either version 3 of the License, or (at your
  * option) any later version.
  *
  *
  *
  * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
  * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  * License for more details.
  *
  *
  *
  * You should have received a copy of the GNU General Public License along with this program; if not, write to
  * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
  *
  */

#include <cprime/themefunc.h>
#include <QFileInfo>
#include <QMenu>
#include <QProcessEnvironment>
#include <QShortcut>

#include "ctermwidget.h"
#include "global.h"

CTermWidget::CTermWidget(settings *s, QWidget *parent, const QString &wDir, const QString &cmd)
    : QTermWidget(0, parent)
    , smi(s)
{
    basicInit();

    if (wDir.length()) {
        setWorkingDirectory(wDir);
    }

    if (cmd.length()) {
        setArgs(QStringList() << "-l" << "-c" << cmd);
    }

    startShellProgram();

    globalObj::watcher()->addPath(QString("/proc/%1/").arg(getShellPID()));
    oldCWD = QFileInfo(QString("/proc/%1/cwd").arg(getShellPID())).symLinkTarget();
    connect(globalObj::watcher(),
            &QFileSystemWatcher::directoryChanged,
            this,
            &CTermWidget::handleFSWSignals);

    setFocus();
}

void CTermWidget::basicInit()
{
    loadSettings();

    /* Set the enivronment variable TERM as xterm */
    QProcessEnvironment procEnv = QProcessEnvironment::systemEnvironment();

    if (!termEnv.length()) {
        termEnv = "xterm-256color";
    }

    procEnv.insert("TERM", termEnv);
    setEnvironment(procEnv.toStringList());

    setColorScheme(terminalColorScheme);
    setKeyBindings(terminalKeyTab);

    setScrollBarPosition(QTermWidget::ScrollBarRight);

    setTerminalFont(terminalFont);
    setHistorySize(terminalHistorySize);
#ifndef OLD_QTERMWIDGET
    setKeyboardCursorShape(
        (QTermWidget::KeyboardCursorShape)(int) smi->getValue("CoreTerminal", "CursorShape"));
#endif
    setShellProgram(terminalShell);

    setMotionAfterPasting(2);
    setFlowControlEnabled(true);
    setFlowControlWarningEnabled(true);

    setTerminalOpacity(1.0 * terminalOpacity / 100);

    setTerminalSizeHint(false);
    setBlinkingCursor(true);

    /* Actions */
    copyAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-copy-symbolic",
                                                       "edit-copy",
                                                       "edit-copy"),
                          "Copy",
                          this);
    connect(copyAct, SIGNAL(triggered()), this, SLOT(copyClipboard()));

    pasteSelAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-paste-symbolic",
                                                           "edit-paste",
                                                           "edit-paste"),
                              "Paste Selection",
                              this);
    connect(pasteSelAct, SIGNAL(triggered()), this, SLOT(pasteSelection()));

    pasteClipAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-paste-symbolic",
                                                            "edit-paste",
                                                            "edit-paste"),
                               "Paste Clipboard",
                               this);
    connect(pasteClipAct, SIGNAL(triggered()), this, SLOT(pasteClipboard()));

    clearAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-clear-symbolic",
                                                        "edit-clear",
                                                        "edit-clear"),
                           "Clear Terminal",
                           this);
    connect(clearAct, SIGNAL(triggered()), this, SLOT(clear()));

    searchAct = new QAction(CPrime::ThemeFunc::themeIcon("edit-find-symbolic",
                                                         "edit-find",
                                                         "edit-find"),
                            "Search Terminal",
                            this);
    connect(searchAct, SIGNAL(triggered()), this, SLOT(toggleShowSearchBar()));

    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this,
            &QTermWidget::customContextMenuRequested,
            this,
            &CTermWidget::showCustomContextMenu);

    /* Shortcuts */
    QShortcut *shortcut;
    shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_Plus), this);
    connect(shortcut, &QShortcut::activated, this, &CTermWidget::zoomIn);

    shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::Key_Minus), this);
    connect(shortcut, &QShortcut::activated, this, &CTermWidget::zoomOut);
}

QString CTermWidget::currentWorkingDirectory()
{
    QString cwd = QString("/proc/%1/cwd").arg(getShellPID());

    return QFileInfo(cwd).symLinkTarget();
}

void CTermWidget::wheelEvent(QWheelEvent *event)
{
    QPoint wheelScroll = event->angleDelta();

    if (event->modifiers() & Qt::ControlModifier) {
        if ((wheelScroll.x() > 0) || (wheelScroll.y() > 0)) {
            this->zoomIn();
        } else {
            this->zoomOut();
        }

        return;
    }

    QTermWidget::wheelEvent(event);
}

void CTermWidget::loadSettings()
{
    terminalFont = smi->getValue("CoreTerminal", "Font");
    terminalOpacity = smi->getValue("CoreTerminal", "Opacity");
    terminalHistorySize = smi->getValue("CoreTerminal", "HistorySize");
    terminalKeyTab = static_cast<QString>(smi->getValue("CoreTerminal", "KeyTab"));
    terminalShell = static_cast<QString>(smi->getValue("CoreTerminal", "Shell"));
    terminalColorScheme = static_cast<QString>(smi->getValue("CoreTerminal", "ColorScheme"));
    termEnv = static_cast<QString>(smi->getValue("CoreTerminal", "TERM"));
}

void CTermWidget::handleFSWSignals(const QString &)
{
    if (QFileInfo(QString("/proc/%1/cwd").arg(getShellPID())).symLinkTarget() == oldCWD) {
        return;
    }

    oldCWD = QFileInfo(QString("/proc/%1/cwd").arg(getShellPID())).symLinkTarget();
    emit chDir(oldCWD);
}

void CTermWidget::showCustomContextMenu(const QPoint &pos)
{
    QMenu *menu = new QMenu(this);

    menu->addAction(copyAct);
    menu->addAction(pasteSelAct);
    menu->addAction(pasteClipAct);
    menu->addSeparator();
    menu->addAction(clearAct);
    menu->addSeparator();
    menu->addAction(searchAct);
    menu->exec(mapToGlobal(pos));
}

void CTermWidget::zoomIn()
{
    setTerminalSizeHint(true);
    if (getTerminalFont().pointSize() < 48) {
        QTermWidget::zoomIn();
    }

    setTerminalSizeHint(false);
}

void CTermWidget::zoomOut()
{
    setTerminalSizeHint(true);
    if (getTerminalFont().pointSize() > 7) {
        QTermWidget::zoomOut();
    }

    setTerminalSizeHint(false);
}
